package com.example.homework2.Model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@AllArgsConstructor
public class Article {
    private int id;
    private String title;
    private String description;
    private String image;
}
