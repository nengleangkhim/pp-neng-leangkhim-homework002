package com.example.homework2.Controller;

import com.example.homework2.Model.Article;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@Controller
public class ArticleController {

    @GetMapping("/Home")
    public String index(){
        return "index";
    }

    @GetMapping("/Create-Form")
    public String CreateForm(){
        return "CreateForm";
    }
    @GetMapping("/View")
    public String ViewForm(){
        return "View";
    }
}
